Remove-Variable * -ErrorAction SilentlyContinue
Start-Process "C:\Program Files\Blue Prism Limited\Blue Prism Automate\automatec.exe" -NoNewWindow -Wait -PassThru -ArgumentList "/run ""CreateOrders"" /resource DESKTOP-0GUMLBC /user admin admin1234 /dbconname DEV" -RedirectStandardOutput "output1.txt" -WorkingDirectory "C:\Users\hvard\.jenkins\workspace\Blueprism\Blueprism_Process_Test" 
Get-Content "output1.txt" | Foreach-Object {
   $var = $_.Split(':')
   New-Variable -Name $var[0] -Value $var[1]
}
$argList = "/user admin admin1234 /dbconname DEV /status ";
$argList = $argList + $Session;
New-Variable -Name Status -Value "Running";
While ($Status -eq "Running")
{
                Start-Sleep -s 10;
                Start-Process "C:\Program Files\Blue Prism Limited\Blue Prism Automate\automatec.exe" -ArgumentList $argList -NoNewWindow -Wait -RedirectStandardOutput "output2.txt" -WorkingDirectory "C:\Users\hvard\.jenkins\workspace\Blueprism\Blueprism_Process_Test";
                Get-Content "output2.txt" | Foreach-Object {
                                $var = $_.Split(':')
                                Remove-Variable -Name $var[0]
                                New-Variable -Name $var[0] -Value $var[1]
                }
}
if ($Status -eq "Completed")
{
                exit 0;
}
else
{
                exit 1;
}
$LastExitCode;
